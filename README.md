# My OkCupid Project

This project is a coding exercise for OkCupid. Double Tap to like a Match.

## API Usage

	- Used a sample match API from OkCupid.
	
## Third Party SDKs Used

	- Used several third party SDKs that I like to use in my own projects.
	- Retrofit for making REST calls.
	- OKHTTP as an HTTP client.
	- MaterialProgressBar for beautiful loading.
	- Fresco for image loading / caching.