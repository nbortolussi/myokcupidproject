package com.myokcupidproject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by nbortolussi on 5/27/16.
 */
public class RestClient {

    private static OkCupidInterface okCupidInterface;

    public static OkCupidInterface getOkCupidInterface() {
        if (okCupidInterface == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://www.okcupid.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            okCupidInterface = retrofit.create(OkCupidInterface.class);
        }

        return okCupidInterface;
    }

    public interface OkCupidInterface {

        @GET("matchSample.json")
        Call<MatchResponse> getMatchSample();
    }
}
