package com.myokcupidproject;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by nbortolussi on 5/27/16.
 */
public class MyDoubleTapView extends LinearLayout {
    GestureDetector gestureDetector;

    public MyDoubleTapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return gestureDetector.onTouchEvent(e);
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            MatchResponse.Match match = (MatchResponse.Match) getTag();
            if (match.isLiked()) {
                match.setLiked(false);
                findViewById(R.id.heart).setVisibility(View.GONE);
            } else {
                match.setLiked(true);
                findViewById(R.id.heart).setVisibility(View.VISIBLE);
            }

            return true;
        }
    }
}

