package com.myokcupidproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;


public class MatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MatchResponse.Match> data;

    public MatchAdapter() {
        this.data = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MatchViewHolder matchViewHolder = (MatchViewHolder) viewHolder;
        MatchResponse.Match match = data.get(position);
        matchViewHolder.avatar.setImageURI(match.getAvatarUri());
        matchViewHolder.agelocation.setText(match.getAgeLocationString());
        matchViewHolder.matchpercent.setText(match.getMatchPercentage());
        matchViewHolder.username.setText(match.getUsername());
        if (match.isLiked())
            matchViewHolder.heart.setVisibility(View.VISIBLE);
        else
            matchViewHolder.heart.setVisibility(View.GONE);
        matchViewHolder.myDoubleTapView.setTag(match);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_match, viewGroup, false);
        return new MatchViewHolder(itemView);
    }

    public static class MatchViewHolder extends RecyclerView.ViewHolder {
        protected SimpleDraweeView avatar;
        protected TextView username;
        protected TextView agelocation;
        protected TextView matchpercent;
        protected ImageView heart;
        protected MyDoubleTapView myDoubleTapView;

        public MatchViewHolder(View v) {
            super(v);
            avatar = (SimpleDraweeView) v.findViewById(R.id.avatar);
            username = (TextView) v.findViewById(R.id.username);
            agelocation = (TextView) v.findViewById(R.id.agelocation);
            matchpercent = (TextView) v.findViewById(R.id.matchpercent);
            heart = (ImageView) v.findViewById(R.id.heart);
            myDoubleTapView = (MyDoubleTapView) v.findViewById(R.id.mydoubletapview);
        }
    }

    public void addData(ArrayList<MatchResponse.Match> data) {
        this.data.addAll(data);
        notifyItemRangeChanged(this.data.size() - data.size(), data.size());
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }
}