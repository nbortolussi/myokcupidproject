package com.myokcupidproject;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class MainActivity extends AppCompatActivity {

    private GetSampleMatches getSampleMatches;
    private MaterialProgressBar loadingprogress;
    private MatchAdapter matchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActivity();
        getSampleMatches();
    }

    public void setupActivity() {
        loadingprogress = (MaterialProgressBar) findViewById(R.id.loadingprogress);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.matchesrv);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        matchAdapter = new MatchAdapter();
        recyclerView.setAdapter(matchAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getSampleMatches != null)
            getSampleMatches.cancel(true);
    }

    public void getSampleMatches() {
        getSampleMatches = new GetSampleMatches();
        getSampleMatches.execute();
    }

    public class GetSampleMatches extends AsyncTask<Void, Void, Boolean> {

        private MatchResponse matchResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingprogress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voided) {
            try {
                matchResponse = RestClient.getOkCupidInterface().getMatchSample().execute().body();
                return true;
            } catch (IOException e) {
                Log.e(getClass().getName(), "Oops, our call to get matches failed.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success)
                addMatchResponseToAdapter(matchResponse);
            else
                Toast.makeText(MainActivity.this, getString(R.string.fail), Toast.LENGTH_SHORT).show();

            loadingprogress.setVisibility(View.GONE);
        }
    }

    public void addMatchResponseToAdapter(MatchResponse matchResponse) {
        if (matchResponse != null && matchResponse.getMatches() != null)
            matchAdapter.addData(matchResponse.getMatches());
    }
}
