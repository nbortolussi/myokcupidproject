package com.myokcupidproject;

import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by nbortolussi on 5/27/16.
 */
public class MatchResponse {
    private int total_matches;
    private ArrayList<Match> data;

    public ArrayList<Match> getMatches() {
        return data;
    }

    public void setMatches(ArrayList<Match> data) {
        this.data = data;
    }

    public int getTotalMatches() {
        return total_matches;
    }

    public void setTotalMatches(int total_matches) {
        this.total_matches = total_matches;
    }

    public static class Match {
        private String username;
        private String city_name;
        private int age;
        private String state_code;
        private float match;
        private PhotoInfo photo;
        private boolean isLiked;

        public boolean isLiked() {
            return isLiked;
        }

        public void setLiked(boolean liked) {
            isLiked = liked;
        }

        public String getUsername() {
            return username;
        }

        public String getAgeLocationString() {
            return age + " \u2022 " + city_name + ", " + state_code;
        }

        public String getMatchPercentage() {
            return Math.round(match / 100) + "%";
        }

        public Uri getAvatarUri() {
            return Uri.parse(photo.thumb_paths.desktop_match);
        }

        public static class PhotoInfo {
            private ThumbPaths thumb_paths;

            public static class ThumbPaths {
                private String desktop_match;
            }
        }
    }
}
